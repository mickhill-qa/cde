
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div><!-- #END# Page Loader -->
        
        
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div><!-- #END# Overlay For Sidebars -->
        
        
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="./">CDE - Controle De Equipamento</a>
                </div>
                <div class="collapse navbar-collapse align-center" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Call Search -->
                        <li>
                            <a href="romaneio-novo" class="btn bg-amber waves-effect">
                                <i class="material-icons">input</i>
                                <span>Novo Romaneio</span>
                            </a>
                        </li>
                        <!-- #END# Call Search -->
                    </ul>
                </div>
            </div>
        </nav><!-- #Top Bar -->
        
        
        <!-- Left panel -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="images/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mick Hill</div>
                        <div class="email">Patrimônio</div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">Fluxo do Processo</li>
                        <li class="active">
                            <a href="romaneio-lista" class=" waves-effect waves-block">
                                <i class="material-icons">list</i>
                                <span>Romaneio</span>
                            </a>
                        </li>
                        <li>
                            <a href="transferencias" class=" waves-effect waves-block">
                                <i class="material-icons">input</i>
                                <span>Transferencia</span>
                            </a>
                        </li>
                        <li>
                            <a href="transferencias" class=" waves-effect waves-block">
                                <i class="material-icons">airport_shuttle</i>
                                <span>Transporte</span>
                            </a>
                        </li>
                        <li>
                            <a href="transferencias" class=" waves-effect waves-block">
                                <i class="material-icons">store</i>
                                <span>Chegada na Loja</span>
                            </a>
                        </li>
                        <li>
                            <a href="transferencias" class=" waves-effect waves-block">
                                <i class="material-icons">swap_vertical_circle</i>
                                <span>Pendente de Retorno</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        2018 &copy; <a href="javascript:void(0);">Mick Hill - Developmente</a>.
                    </div>
                    <div class="version">
                        <b>Version: </b> 1.0
                    </div>
                </div>
                <!-- #Footer -->
            </aside>
            <!-- #END# Left Sidebar -->
        </section><!-- #END# Left panel -->


