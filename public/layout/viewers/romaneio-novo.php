<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>CDE | Novo Romaneio</title>
        <!-- Favicon-->
        <link rel="icon" href=images/fav.png type="image/x-png">

        <!-- Google Fonts -->
        <link href="google/css.css" rel="stylesheet" type="text/css">
        <link href="google/icon.css" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

        <!-- Wait Me Css -->
        <link href="plugins/waitme/waitMe.css" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="css/themes/all-themes.css" rel="stylesheet" />
    </head>

    <body class="theme-blue">
        <?php include $pasta_layout . "painel.php"; ?>

        
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>Controle de envio de equipamentos</h2>
                </div>
                <!-- Input -->
                <form action="/cde/" method="POST" class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Romaneio Thallyson
                                    <small>Equipamentos de Informática</small>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <h2 class="card-inside-title">Informações do Chamado</h2>
                                <div class="row clearfix">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="chamado_numero" placeholder="NUMERO DO CHAMADO">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control show-tick" name="chamado_tipo" required>
                                            <option value="" >-- TIPO DE CHAMADO --</option>
                                            <option value="0">E-MAIL</option>
                                            <option value="1">OCOMON</option>
                                            <option value="2">PG-C</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control show-tick" name="chamado_natureza" required>
                                            <option value="">-- TIPO ROMANEIO --</option>
                                            <option value="1">INCLUSÃO</option>
                                            <option value="2">SUBSTITUIÇÃO</option>
                                            <option value="4">EMPRESTIMO</option>
                                        </select>
                                    </div>
                                </div>
                                <h2 class="card-inside-title">Infomações do destinatário</h2>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <select class="form-control show-tick" name="destino_loja" required>
                                            <option value="" class="active">-- DESTINO --</option>
                                            <option value="1">LJ01 - ILDEFONSO ALBANO</option>
                                            <option value="2">LJ02 - SERRINHA</option>
                                            <option value="3">LJ03 - OLIVEIRA PÁIVA</option>
                                            <option value="4">LJ04 - </option>
                                            <option value="5">LJ05 - JOSÉ WALTER</option>
                                            <option value="6">LJ06 - PINTO MADEIRA</option>
                                            <option value="7">LJ07 - </option>
                                            <option value="8">LJ08 - </option>
                                            <option value="9">LJ09 - MARACANÚ</option>
                                            <option value="10">LJ10 - JULIO LIMA</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control show-tick" name="destino_setor" required>
                                            <option value="">-- SETOR --</option>
                                            <option value="">FRENTE DE LOJA</option>
                                            <option value="10">MONITORAMENTO</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="destino_local" placeholder="LOCAL"  style="text-transform:uppercase;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="card-inside-title">Informações do(s) Equipamento(s)</h2>
                                <div class="row clearfix">
                                    <!--<div class="body">-->
                                    <table class="table table-striped">
                                        <tbody id="list">
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th><input type="text" class="form-control" id="eq_nome" placeholder="EQUIPAMENTO"></th>
                                                <th><input type="text" class="form-control" id="eq_tombo" placeholder="TOMBO OU N/S"></th>
                                                <th style="text-align:  center !important;">
                                                    <button type="button" id="addbtn" name="addbtn" value="Add" class="btn btn-primary btn-circle waves-effect waves-circle waves-float">
                                                        <i class="material-icons">keyboard_return</i>
                                                    </button></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <!--</div>-->
                                </div>


                                <div class="row clearfix">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="demo-button">
                                            <button type="button" class="btn btn-block btn-lg waves-effect">SALVAR COMO RASCUNHO</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="demo-button">
                                            <button type="submit" class="btn btn-block btn-lg btn-primary waves-effect">CONCLUIR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <!-- Jquery Core Js -->
        <script src="plugins/jquery/jquery.min.js"></script>
        
        <!-- Add item a tabela no Romaneio -->
        <script>
            $('#addbtn').click(function(){
                    var nome_ed     = $('#eq_nome').val();
                    var tombo_eq    = $('#eq_tombo').val();
                    var uniqid      = Math.round(new Date().getTime() + (Math.random() * 100));


                    $('#list').append('\
                        <tr id="'+uniqid+'">\n\
                            <td><input type="text" class="form-control" name="item_nome[]" placeholder="VAZIO" value="'+nome_ed+'" required></td>\n\
                            <td><input type="text" class="form-control" name="item_tombo[]" placeholder="VAZIO" value="'+tombo_eq+'"></td>\n\
                            <td align="center">\n\
                                <button type="button" data-id="'+uniqid+'" class="listelement btn btn-danger btn-circle waves-effect waves-circle waves-float">\n\
                                    <i class="material-icons">close</i>\n\
                                </button>\n\
                            </td>\n\
                        </tr>'
                    );
                    $('#eq_nome').val('');
                    $('#eq_tombo').val('');
                    return false;
            });
            $('#list').delegate(".listelement", "click", function() {
                var elemid = $(this).attr('data-id');
                $("#"+elemid).remove();
            });
        </script>

        <!-- Bootstrap Core Js -->
        <script src="plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js -->
        <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="plugins/node-waves/waves.js"></script>

        <!-- Autosize Plugin Js -->
        <script src="plugins/autosize/autosize.js"></script>

        <!-- Moment Plugin Js -->
        <script src="plugins/momentjs/moment.js"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Custom Js -->
        <script src="js/admin.js"></script>
        <script src="js/pages/forms/basic-form-elements.js"></script>

        <!-- Demo Js -->
        <script src="js/demo.js"></script>
    </body>
</html>
