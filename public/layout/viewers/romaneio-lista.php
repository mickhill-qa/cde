
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>CDE | Lista de Romaneios</title>
        <!-- Favicon-->
        <link rel="icon" href=images/fav.png type="image/x-png">

        <!-- Google Fonts -->
        <link href="google/css.css" rel="stylesheet" type="text/css">
        <link href="google/icon.css" rel="stylesheet" type="text/css">
        
        <!-- Bootstrap Core Css -->
        <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="css/themes/all-themes.css" rel="stylesheet" />
    </head>

    <body class="theme-blue">
        <?php include $pasta_layout . "painel.php"; ?>       

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>Equipamentos colocados na área de despacho hoje</h2>
                </div>
                
                <!-- Striped Rows -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    STRIPED ROWS
                                    <small>Use <code>.table-striped</code> to add zebra-striping to any table row within the <code>&lt;tbody&gt;</code></small>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>DESTINO</th>
                                            <th>CHAMADO</th>
                                            <th>CRIADO</th>
                                            <th>STATUS</th>
                                            <th>AÇÕES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td data-toggle="tooltip" data-placement="top" title="" data-original-title="IDELFONSO ALBANO">01 - MTZ</td>
                                            <td>
                                                <a target="_blank" href="http://192.168.7.55/ocomon/geral/mostra_relatorio_individual.php?numero=57087" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abrir">OCOMON 57087</a>
                                            </td>
                                            <td>31/07/2018 ás 10:25</td>
                                            <td>
                                                <a href="romaneio-editar?n=1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">CRIADO</a>
                                            </td>
                                            <td>
                                                <a href="romaneio-buscar?n=1" class="btn bg-blue btn-block btn-xs waves-effect">Ver</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td data-toggle="tooltip" data-placement="top" title="" data-original-title="JULIO LIMA">10 - JUL</td>
                                            <td>PG-C</td>
                                            <td>31/07/2018 ás 10:27</td>
                                            <td>
                                                <a href="romaneio-editar?n=2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">CRIADO</a>
                                            </td>
                                            <td>
                                                <a href="http://google.com" target="_blank" class="btn bg-blue btn-block btn-xs waves-effect">Ver</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td data-toggle="tooltip" data-placement="top" title="" data-original-title="SIQUEIRA">23 - SIQ</td>
                                            <td>E-MAIL</td>
                                            <td>31/07/2018 ás 10:29</td>
                                            <td>
                                                <a href="romaneio-editar?n=3" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">CRIADO</a>
                                            </td>
                                            <td>
                                                <a href="http://google.com" target="_blank" class="btn bg-blue btn-block btn-xs waves-effect">Ver</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td data-toggle="tooltip" data-placement="top" title="" data-original-title="OLIVEIRA PAIVA">03 - OLI</td>
                                            <td>E-MAIL</td>
                                            <td>31/07/2018 ás 10:31</td>
                                            <td>TRANFERIDO</td>
                                            <td>
                                                <a href="http://google.com" target="_blank" class="btn bg-blue btn-block btn-xs waves-effect">Ver</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td data-toggle="tooltip" data-placement="top" title="" data-original-title="CD SERRINHA">07 - CD</td>
                                            <td>
                                                <a target="_blank" href="http://192.168.7.55/ocomon/geral/mostra_relatorio_individual.php?numero=57087" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abrir">OCOMON 57087</a>
                                            </td>
                                            <td>31/07/2018 ás 10:33</td>
                                            <td>TRANFERIDO</td>
                                            <td>
                                                <a href="http://google.com" target="_blank" class="btn bg-blue btn-block btn-xs waves-effect">Ver</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Striped Rows -->
            </div>
        </section>

        <!-- Jquery Core Js -->
        <script src="plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js -->
        <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="plugins/node-waves/waves.js"></script>

        <!-- Custom Js -->
        <script src="js/admin.js"></script>
        <script src="js/pages/ui/tooltips-popovers.js"></script>

        <!-- Demo Js -->
        <script src="js/demo.js"></script>
        
    </body>

</html>
